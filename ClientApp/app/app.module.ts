import { FocusOnInit } from './directives/focus-on-init.directive';
import * as Raven from 'raven-js';
import { ErrorHandler } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { PopupMessageModule } from './popup-message/popup-message.module';
import { AppErrorHandler } from './services/app.error-handler';
import { CommonService } from './services/common.service';
import { NoteService } from './services/note.service';
import { KeywordService } from './services/keyword.service';
import { PopupMessageService } from './popup-message/popup-message.service';
import { KeywordDropdownFormComponent } from './components/keyword-dropdown/keyword-dropdown-form.component';
import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { NoteFormComponent } from './components/note-form/note-form.component';
import { NoteListComponent } from './components/note-list/note-list.component';
import { KeywordEditFormComponent } from './components/keyword-edit-form/keyword-edit-form.component';
import { KeywordListComponent } from './components/keyword-list/keyword-list.component';

Raven
    .config('https://33d5b2d44c3043ea89bcb851a0f360f8@sentry.io/228768')
    .install();

export class RavenErrorHandler implements ErrorHandler {
    handleError(err: any): void {
        Raven.captureException(err);
    }
}

@NgModule({
    declarations: [
        AppComponent,
        FocusOnInit,
        NavMenuComponent,
        NoteFormComponent,
        NoteListComponent,
        KeywordEditFormComponent,
        KeywordListComponent,
        KeywordDropdownFormComponent,
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        PopupMessageModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'notes', pathMatch: 'full' },
            { path: 'notes/new', component: NoteFormComponent },
            { path: 'notes', component: NoteListComponent },
            { path: 'notes/:id', component: NoteFormComponent },
            { path: 'keywords', component: KeywordListComponent },
            { path: '**', redirectTo: 'notes' }
        ])
    ],
    providers: [
        { provide: ErrorHandler, useClass: AppErrorHandler },
        CommonService,
        KeywordService,
        NoteService,
        PopupMessageService,

    ]
})

export class AppModuleShared { }