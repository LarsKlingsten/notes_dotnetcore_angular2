import { PopupMessage } from './../../popup-message/popup-message.model';
import { Keyword } from './../../models/keyword.model';
import { CommonService } from './../../services/common.service';
import { KeywordService } from './../../services/keyword.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-keyword-dropdown-form',
  templateUrl: './keyword-dropdown-form.component.html',
  styleUrls: ['./keyword-dropdown-form.component.css']
})
export class KeywordDropdownFormComponent implements OnInit {

 
  @Input() keywords: Keyword[];
  @Output() onAddExistingKeyword: EventEmitter<Keyword> = new EventEmitter<Keyword>();
  displayKeywordEditForm: boolean = false;

  constructor(private common: CommonService) { }

  ngOnInit() {
    this.common.setTitle("Select Keyword")
  }

  ngOnDestroy() {
    this.common.setTitleToPrev();
  }
  onCancel() {
    this.onAddExistingKeyword.emit(undefined);
  }

  onKeywordAdd(id: number) {
    let keyword: Keyword = this.keywords.find(k => k.id == id) || { id: -1, name: "keyword not found" };
    this.onAddExistingKeyword.emit(keyword);
  }

  addKeyword(keyword: Keyword) {
    this.removeKeyword(keyword);
    this.keywords.push(keyword);
    this.sortKeywords();
  }

  removeKeyword(keyword: Keyword) {
    this.keywords = this.keywords.filter(item => item.id !== keyword.id);
  }

  sortKeywords() { /** sort keywords by name **/
    this.keywords.sort(function (a, b) {
      if (a.name > b.name) { return 1; }
      if (a.name < b.name) { return -1; }
      return 0;
    });
  }
}