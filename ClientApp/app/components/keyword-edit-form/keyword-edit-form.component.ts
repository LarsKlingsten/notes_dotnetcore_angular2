import { Component, EventEmitter, Input, OnInit, Output, Renderer, AfterViewInit } from '@angular/core';
import { PopupMessage } from './../../popup-message/popup-message.model';
import { Keyword } from './../../models/keyword.model';
import { CommonService } from './../../services/common.service';
import { KeywordService } from './../../services/keyword.service';

@Component({
  selector: 'app-keyword-edit-form',
  templateUrl: './keyword-edit-form.component.html',
  styleUrls: ['./keyword-edit-form.component.css']
})


/**
 * Input: keyword (null value to add) 
 * Output: keywordAdd(keyword)  or null for user cancelled
 * Output: keyworddelete 
 
 * Add, update, delete are updated directly to database from this component
 */
export class KeywordEditFormComponent implements OnInit, AfterViewInit {

  @Input() keyword: Keyword;
  @Output() keywordAdd: EventEmitter<Keyword> = new EventEmitter<Keyword>();
  @Output() keywordDelete: EventEmitter<Keyword> = new EventEmitter<Keyword>();

  orgKeyword: Keyword;

  constructor(
    private keywordService: KeywordService,
    private common: CommonService,
    private renderer: Renderer
  ) { }

  ngOnInit() {
    if (!this.keyword) {
      this.keyword = { id: 0, name: '' };
    } else {
      this.orgKeyword = { id: this.keyword.id, name: this.keyword.name };  // copy the keyword object
    }
    this.common.setTitle(this.NewOrEditKeyword);
  }

  ngAfterViewInit() {
    this.renderer.selectRootElement('#inputName').focus();
  }

  ngOnDestroy() {
    this.common.setTitleToPrev();
  }

  onSubmit() {
    if (this.keyword.id > 0) { // editing an existing item  
      this.keywordService.update(this.keyword)
        .subscribe(res => {
          this.common.sendPopupMessage(new PopupMessage(res.msg))
          this.keyword = res.data;
          this.keywordAdd.emit(this.keyword);
        });
    }
    else { // creating a new item   
      this.keywordService.create(this.keyword)
        .subscribe(res => {
          this.common.sendPopupMessage(new PopupMessage(res.msg));
          this.keyword = res.data;
          this.keywordAdd.emit(this.keyword);
        })
    }
  }

  onDelete() {
    if (confirm(`Please confirm you wish to delete note ${this.keyword.id}?`)) {
      this.keywordService.delete(this.keyword.id)
        .subscribe(res => {
          this.common.sendPopupMessage(new PopupMessage(res.msg));
          this.keywordDelete.emit(this.keyword);
        })
    }
  }

  onCancel() {
    if (this.orgKeyword) {  // restore the original keyword
      this.keyword = this.orgKeyword
    }
    this.keywordAdd.emit(this.keyword);
  }

  get NewOrEditKeyword() {
    return this.keyword.id > 0 ? "Edit Keyword" : "Add New Keyword";
  }
}
