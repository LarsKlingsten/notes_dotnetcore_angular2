import { Column } from './../../models/column.model';
import { Keyword } from './../../models/keyword.model';
import { CommonService } from './../../services/common.service';
import { KeywordService } from '../../services/keyword.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-keyword-list',
  templateUrl: './keyword-list.component.html',
  styleUrls: ['./keyword-list.component.css']
})
export class KeywordListComponent implements OnInit {

  keyword: Keyword;
  keywords: Keyword[];
  displayKeywordEditForm: boolean = false;

  columns: Column[] = [
    { title: 'Id', key: 'id', isSortable: false },
    { title: 'Name', key: 'name', isSortable: true }
  ];

  constructor(private keywordService: KeywordService, private common: CommonService) { }

  ngOnInit() {
    this.common.setTitle("Keywords List")
    this.keywordService.getKeywords().subscribe(res => {
      this.keywords = res.data;
    });
  }

  ngOnDestroy() {
    this.common.setTitleToPrev();
  }

  onKeywordEditForm(k: Keyword) {
    this.keyword = k;
    this.displayKeywordEditForm = true;
  }

  onKeywordFormAdd(k: Keyword): void {
    if (k != undefined) {
      this.addKeyword(k);
    }
    this.displayKeywordEditForm = false;
  }

  onKeywordFormDelete(k: Keyword): void {
    this.removeKeyword(k);
    this.displayKeywordEditForm = false;
  }


  addKeyword(keyword: Keyword) {
    this.removeKeyword(keyword);
    this.keywords.push(keyword);
    this.sortKeywords();
  }

  removeKeyword(keyword: Keyword) {
    this.keywords = this.keywords.filter(item => item.id !== keyword.id);
  }

  sortKeywords() { /** sort keywords by name **/
    this.keywords.sort(function (a, b) {
      if (a.name > b.name) { return 1; }
      if (a.name < b.name) { return -1; }
      return 0;
    });
  }
}
