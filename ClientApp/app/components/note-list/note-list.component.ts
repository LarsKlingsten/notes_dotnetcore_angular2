import { Column } from './../../models/column.model';
import { Note } from './../../models/note.model';
import { CommonService } from './../../services/common.service';
import { NoteService } from './../../services/note.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {

  notes: Note[];
  routerLink: any;
  search: any;
  columns: Column[];

  constructor(private noteService: NoteService, private common: CommonService) {
    this.columns = [
      { title: 'Id', key: 'id', isSortable: false },
      { title: 'Subject', key: 'subject', isSortable: true }]
  }

  ngOnInit() {
    this.noteService.getNotes().subscribe(res => {
      this.notes = res.data;
    });
  }

  onSearch() {
    this.noteService.getNotesByKeyword(this.search).subscribe(res => {
      this.notes = res.data;
    })
  }
}