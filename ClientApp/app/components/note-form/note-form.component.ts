import { Observable } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CommonService } from '../../services/common.service';
import { KeywordService } from './../../services/keyword.service';
import { NoteService } from './../../services/note.service';
import { Note } from './../../models/note.model';
import { ServerResponse } from './../../models/serverResponse.model';
import { Keyword } from './../../models/keyword.model';

@Component({
  selector: 'app-note-form',
  templateUrl: './note-form.component.html',
  styleUrls: ['./note-form.component.css']
})

export class NoteFormComponent implements OnInit, OnDestroy {

  @ViewChild('myForm') myForm: NgForm;

  allKeywords: Keyword[];
  note: Note;
  private readonly keywordNotFound: Keyword;
  isDataLoaded: boolean = false;
  isKeywordEditFormShow: boolean = false;
  isKeywordDropdownShow: boolean = false;
  isUserCancelled: boolean = false;
  isFormDirty: boolean = false;

  constructor(private noteService: NoteService,
    private keywordService: KeywordService,
    private common: CommonService,
    private route: ActivatedRoute) {

    // we are adding a new Note object to avoid checking for null at the HTML form
    this.note = this.newNote();

    // get this id from the url parameter, also listen for changes
    route.params.subscribe(p => { this.note.id = +p['id']; })  // we need the '+' to convert string to a number
    this.keywordNotFound = { id: -1, name: "keyword does not exist" };
  }

  ngOnInit() {
    this.common.setTitle("Edit Note")

    /** if the url includes an ID, such as ../notes/6 then
     * we fetch the note (to allow for edits) from the server.
     * If strings are used as ID by a user directly in the URL (which is
     * not the correct usage) - we also skip fetching the note.
     * Strings will be NaN  **/
    let http_get_sources = [this.keywordService.getKeywords()];
    if (this.note.id) { // add also 'getNote(x) http requests
      http_get_sources.push(this.noteService.getNote(this.note.id));
    }

    // handle all http request(s) simultaneously
    Observable.forkJoin(http_get_sources).subscribe(data => {
      this.allKeywords = data[0].data;

      // check again - before setting the data this.note
      if (this.note.id) {
        this.note = data[1].data;
        this.removeDuplicateKeywords(this.note.keywords);
      }
      this.isDataLoaded = true;
    },
      err => {
        /** we need to handle the error here also, as its possible that a 
         * users enters the URL directly, where the global error handlers
         *  has not yet been wired up **/
        this.common.sendPopupMessageStr(`a Note with id=${this.note.id} does not exist.  code=${err.status} text=${err.statusText}`);
        /** navigate to home page
         * if we do not navigate away then set this.note.id = 0 (as we will otherwise /
         * get a 'put' request (see submit() ) ... for new notes instead of  a post request **/
        this.common.navigate();
      })

    // added '!' to avoid typescript strict error 
    this.myForm.valueChanges!.subscribe((value: any) => {
      if (this.myForm.dirty) {
        this.isFormDirty = true;
      }
    });
  }

  async ngOnDestroy() { // also possible save the form data
    /* we do not wish save if user cancelled the update, but
       we do wish to save if the user navigates away.

      Known bug. If the user navigates away from the form without having saved
      - the list on note-list.component does not get properly updated - but the 
      note has been property saved in the db. */
    if (!this.isUserCancelled && this.isFormDirty) {
      const res = await this.saveNoteToDatabase();
      this.common.sendPopupMessageStr(res.msg);
    }
    this.common.setTitleToPrev();
  }

  get NewOrEditNote(): string {
    return this.note.id > 0 ? "Edit Note" : "New Note";
  }

  newNote(): Note {   /** new note with id 0**/
    return { id: 0, body: "", subject: "", updated: this.common.utcNow, created: this.common.utcNow, keywords: [] };
  }

  /** Remove keywords from 'allkeywords' that already are set in note.keywords collections
    *  (so the user can not apply the same keywords twice on a  note) **/
  removeDuplicateKeywords(noteKeywords: Keyword[]) {
    if (this.allKeywords) {
      for (let k of noteKeywords) {
        this.allKeywords = this.allKeywords.filter(keyword => keyword.id !== k.id)
      }
    }
  }


  /** We indirectly save the note by navigating away from the form 
     - which triggers ngOnDestroy() => that calls the saveNoteToDatabase()   */
  onSubmit() {
    this.isFormDirty = true; // we update the note whether or not is dirty - as the user may wish to fresh to updatedDate
    this.common.navigate();
  }

  async saveNoteToDatabase(): Promise<ServerResponse> {
    if (this.note.id > 0) { // editing an existing note -> we use put  
      return await this.noteService.updateNote(this.note).toPromise();
    }
    else { // creating a new note -> we use post 
      return await this.noteService.createNote(this.note).toPromise();
    }
  }

  onNoteDelete() {
    if (confirm(`Please confirm you wish to delete note ${this.note.id}?`)) {
      this.noteService.deleteNote(this.note.id)
        .subscribe(res => {
          this.common.sendPopupMessageStr(res.msg);
          this.common.navigate();
        })
    }
  }

  onButtonCancel() {
    this.common.sendPopupMessageStr("User cancelled update");
    this.isUserCancelled = true; // avoid having the request sent twice on ngOnDestroy
    this.common.navigate();
  }

  // adding existing keyword to note //
  displayKeywordDropdown() {
    this.isKeywordDropdownShow = true;
  }

  onKeywordRemove(keyword: Keyword) {
    this.note.keywords = this.note.keywords.filter(item => item.id !== keyword.id)
  }

  onAddExistingKeyword(k: Keyword) {
    this.isKeywordDropdownShow = false;
    if (k !== undefined) {
      this.common.sendPopupMessageStr("added exsting keyword " + k.name)
      this.note.keywords.push(k);
      this.allKeywords = this.allKeywords.filter(item => item.id !== k.id); // remove keyword from dropdown list (so it can not be added twice)
    } else {
      this.common.sendPopupMessageStr("User cancelled adding keywords");
    }
  }
}
