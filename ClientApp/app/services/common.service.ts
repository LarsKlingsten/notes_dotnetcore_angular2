import { stringifyWithFunctions } from 'preboot';
import { PopupMessage } from './../popup-message/popup-message.model';
import { PopupMessageService } from './../popup-message/popup-message.service';
import { Inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Injectable()
export class CommonService {

  public readonly baseUrl: string;
  private readonly titlePrg = "Notes"
  private titleTabOld: string = "";

  constructor( @Inject('BASE_URL') baseUrl: string,
    private popupMessageService: PopupMessageService,
    private router: Router,
    private titleService: Title) {
    this.baseUrl = baseUrl;
    this.setTitle("Main");
  }

  utcNowToString(): string {
    return this.formatDateUTC(new Date())
  };


  get utcNow(): Date {
    return new Date()
  };

  now(): string {
    return this.formatDate(new Date());
  }

  formatDateUTC(d: Date): string {
    return `${d.getUTCHours()}:${d.getUTCMinutes()}:${d.getUTCSeconds()}.${d.getUTCMilliseconds()}`;

  }
  formatDate(d: Date): string {
    return `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}.${d.getMilliseconds()}`;
  }

  public sendPopupMessageStr(msg: string, duration: number = 3000) {
    this.popupMessageService.newPopupMessage(new PopupMessage(msg = msg, duration = duration));
  }

  public sendPopupMessage(popupMessage: PopupMessage) {
    this.popupMessageService.newPopupMessage(popupMessage);
  }

  public httpCodeToMessage(error: any): string {
    switch (error.status) {
      case 0: return "Fatal network error";
      case 200: return "Success";
      case 401: "You are not authorized to access this site.";
      case 500: "Internal Server Error";
      default: return error;
    }
  }
  /** navigate to url (default='/') **/
  public navigate(url: string = '/'): void {
    this.router.navigate([url]);
  }

  /** set the title (and store the previous title, allowing to reset with setTitleToPrev() */
  public setTitle(newTitle: string): void {
    this.titleTabOld = this.titleService.getTitle();
    this.titleService.setTitle(this.titlePrg + ":" + newTitle);
  }

  /** Set the title back to its previous title name */
  public setTitleToPrev(): void {
    this.titleService.setTitle(this.titleTabOld);
  }

  /** write string to console.log localtime hours, minutes, seconds, milliseconds */
  public log(msg: string): void {
    console.log(`${this.now()} ${msg}`);
  }
} 