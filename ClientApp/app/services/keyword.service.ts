import { Keyword } from '../models/keyword.model';
import { Observable } from 'rxjs/Rx';
import { ServerResponse } from './../models/serverResponse.model';
import { observable } from 'rxjs/symbol/observable';
import { CommonService } from './common.service';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'; //get map operator for JSON

@Injectable()
export class KeywordService {
  private readonly url: string = "api/keywords";

  constructor(private common: CommonService, private http: Http) { }

  getKeyword(id: number): Observable<ServerResponse> {
    return this.http.get(this.common.baseUrl + this.url + "/" + id).map(res => res.json());
  }

  getKeywords(): Observable<ServerResponse> {
    return this.http.get(this.common.baseUrl + this.url).map(res => res.json());
  }

  create(k: Keyword): Observable<ServerResponse> {
    k.id = 0; // fix NaN issue
    return this.http.post(this.common.baseUrl + this.url, k).map(res => res.json());
  }

  update(k: Keyword): Observable<ServerResponse> {
    return this.http.put(this.common.baseUrl + this.url + "/" + k.id, k).map(res => res.json());
  }

  delete(id: number): Observable<ServerResponse> {
    return this.http.delete(this.common.baseUrl + this.url + "/" + id).map(res => res.json());
  }



}
