import { ErrorHandler, Injector, Injectable, isDevMode } from '@angular/core';
import * as Raven from 'raven-js';
import { ServerResponse } from '../models/serverResponse.model';
import { CommonService } from './common.service';

@Injectable()
export class AppErrorHandler implements ErrorHandler {

    private common: CommonService;

    /** ErrorHandler can not be used with dependency injection
        Instead use Injector ( injector.get(service) ) from @angular/core
        https://medium.com/@amcdnl/global-error-handling-with-angular2-6b992bdfb59c **/
    constructor(private injector: Injector) { }

    handleError(error: any): void {
        this.common = this.injector.get(CommonService);
        let msg: string = "";

        /** Only capture errors when in production 
            This is enabeld in 'boot.browser.ts' -> enableProdMode()  (module.hot == false)
            Which for dotnet core is set in c# start.cs  see: HotModuleReplacement = true   
            Raven is used by Sentry.io  **/
        if (!isDevMode) {
            Raven.captureException(error.originalError || error)
        }
        try {
            let serverResp: ServerResponse = JSON.parse(error._body);
            this.common.sendPopupMessageStr(serverResp.msg);
            console.log(`@errorHandler: status=${error.status} msg=${serverResp.msg} msgErr=${serverResp.errMsg}`);
        }
        catch (e) {
            console.log(`@app.error.handler. Failed to parse error_body  error='${error}' exception='${e}' -> This is likely a http request error`);
            this.common.sendPopupMessageStr("Error! Client/Server communication issue");
        }
    }
}