import { ServerResponse } from '../models/serverResponse.model';
import { Observable } from 'rxjs/Rx';
import { Note } from './../models/note.model';
import { CommonService } from './common.service';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'; //get map operator for JSON

@Injectable()
export class NoteService {


  private readonly url: string = "api/notes";
 

  constructor(private common: CommonService, private http: Http) {
    
  }

  getNote(id: number): Observable<ServerResponse> {
    return this.http.get(this.common.baseUrl + this.url + "/" + id).map(res => res.json());
  }

  getNotes(): Observable<ServerResponse> {
    return this.http.get(this.common.baseUrl + this.url).map(res => res.json());
  }

  getNotesByKeyword(keyword: string): Observable<ServerResponse> {
    let getUrl = this.common.baseUrl + this.url + "/keywordname/" + keyword;
    return this.http.get(getUrl).map(res => res.json());
  }


  createNote(note: Note): Observable<ServerResponse> {
    note.id = 0; // fix NaN issue
    return this.http.post(this.common.baseUrl + this.url, note).map(res => res.json());
  }

  updateNote(note: Note): Observable<ServerResponse> {
    return this.http.put(this.common.baseUrl + this.url + "/" + note.id, note).map(res => res.json());
  }

  deleteNote(id: number): Observable<ServerResponse> {
    return this.http.delete(this.common.baseUrl + this.url + "/" + id).map(res => res.json());
  }


}
