import { ElementRef, Directive } from '@angular/core';

@Directive({
    selector: '[focusOnInit]'
})

/**
 * use as 'focusOnInit' as per below sample:
 * 
 * usage:
 *      <input [(ngModel)]="keyword.name" id="name" type="text" 
 *             class="form-control" placeholder="add keyword here ..." name="inputName"
 *             required #inputName="ngModel" focusOnInit>
 * 
 * Sadly, only a single elemenent per page including also sub components, modals, can be focused only.
 * If more modals are used, and the focus is requirement 
 * 
 *   ngAfterViewInit() {
 *       this.renderer.selectRootElement('#inputName').focus();
 *    }
 *   
 */
export class FocusOnInit {
    constructor(private elementRef: ElementRef) { }
    ngAfterViewInit() {
        this.elementRef.nativeElement.focus();
    }
}