import { NoteFormComponent } from './../components/note-form/note-form.component';
import { Component, Input, OnDestroy } from '@angular/core';
import { PopupMessageService } from '../popup-message/popup-message.service';
import { Subscription } from 'rxjs/Subscription';
import { PopupMessage } from '../popup-message/popup-message.model';

@Component({
    selector: 'my-popup-message',
    template: ` {{ message }}   `
})

export class PopupComponent {
    message: string; //  removed  @Input() as the component is using observables not inputs

    static msgNumber: number = 0;
    currentMsgNumber: number;

    constructor(private popupService: PopupMessageService) {
        popupService.message.subscribe(
            newMessage => {
                this.setMessage(newMessage);
            });
    }

    private setMessage(popupMsg: PopupMessage) {
        this.message = popupMsg.message;

        // clear the message 
        this.delay(popupMsg.duration).then(() => {
            this.message = "";
        })
    }

    private delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}