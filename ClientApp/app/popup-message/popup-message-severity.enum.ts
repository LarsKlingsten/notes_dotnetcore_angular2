export enum Severity {   
    "info",
    "success",
    "warn",
    "error" 
}