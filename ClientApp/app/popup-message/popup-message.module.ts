
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PopupComponent } from '../popup-message/popup-message.component'
import { PopupMessageService } from './popup-message.service'
 
@NgModule({
    imports: [   // imports makes the exported declarations of other modules available in the current module
        BrowserModule,
    ],
    declarations: [ // are to make directives from the current module available to other directives in the current module. Selectors of directives, components or pipes are only matched against the HTML if they are declared or imported.
         PopupComponent,
    ],
    providers: [ // ==  dependency injection - used to inject the services required by components, directives, pipes in in this  module
        PopupMessageService
    ],
    exports: [  // make them public 
          PopupComponent,
    ]

})
export class PopupMessageModule { }