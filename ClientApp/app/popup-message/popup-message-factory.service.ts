import { PopupMessage } from '../popup-message/popup-message.model';
import { Severity } from '../popup-message/popup-message-severity.enum';

export class PopupMessageFactory {

    public static httpErrorMessage(error: any): string {
        switch (error.status) {
            case 0: return "Fatal network error";
            case 401: "You are not authorized to access this site. " + error;
            default: return error;
        }
    }

    public static msgHttpError(error: any) : PopupMessage {
        let msg: string = PopupMessageFactory.httpErrorMessage(error);
        return new PopupMessage(msg, Severity.error);
    }

    public static msgError(msg: string) : PopupMessage {
        return new PopupMessage(msg, Severity.error);
    }

    public static msgSuccess(msg: string) : PopupMessage {
        return new PopupMessage(msg, Severity.success);
    }

    public static msgInfo(msg: string) : PopupMessage {
        return new PopupMessage(msg, Severity.info);
    }

    public static msgWarning(msg: string) : PopupMessage {
        return new PopupMessage(msg, Severity.warn);
    }
}