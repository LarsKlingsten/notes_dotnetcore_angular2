import { Severity } from '../popup-message/popup-message-severity.enum';

export class PopupMessage {
    constructor(
        public message: string,
        public severity: Severity = Severity.info,
        public duration: number = 3000) { }

    public toString(): string {
        return "msg=" + this.message + "severity=" + Severity[this.severity] + " duration=" + this.duration;
    }

}
