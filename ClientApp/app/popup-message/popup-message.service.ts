
import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { PopupMessage } from '../popup-message/popup-message.model';

@Injectable()
export class PopupMessageService {

    // Observable string sources
    private messageSubject = new Subject<PopupMessage>();
    public message = this.messageSubject.asObservable();

    constructor(private ngZone: NgZone) { }

    // Service message commands
    newPopupMessage(popupMsg: PopupMessage) {
        /** using ngZone to force angular to update
         this appears to be a usuable alternative to using 
         ChangeDetectionStrategy.detectChanges(); **/
        this.ngZone.run(() => {
            this.messageSubject.next(popupMsg);
        });
    }

}