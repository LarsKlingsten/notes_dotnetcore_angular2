import { Keyword } from './keyword.model';

export interface Note {
    id: number;
    subject: string;
    body: string
    updated: Date;
    created: Date;
    keywords: Keyword[];
}