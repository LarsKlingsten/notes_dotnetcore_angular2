export interface ServerResponse {
    isOk: boolean;
    msg: string;
    data: any;
    errMsg: string;
    errMsgId: number;
    id: number;
}




/**
Example:
The ServerResponse object as received from the server should be json object, similar to this:
{
    "isOK": true,
    "msg": "Success retrieving data",
    "errMsg": null,
    "errMsgId": 0,
    "id": 0,
    "data": {
        "id": 24,
        "subject": "Nautilus 4",
        "body": "sudo killall nautilus && (nautilus &)  https://askubuntu.com/questions/770134/nautilus-wont-launch-16-04 534",
        "updated": "2017-10-10T06:34:07.5122854",
        "created": "2017-10-05T14:42:10.3434135",
        "keywords": [
            {   "id": 7,
                "name": "CSharp"
            },
            {   "id": 8,
                "name": "Golang"
            }
        ]
    }  
}


the response object may also be retrieve from the error object, as follows:

let serverResp: ServerResponse = JSON.parse(error._body);
(see app.errorhandler.ts)

 */