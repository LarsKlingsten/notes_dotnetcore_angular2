export interface Column {
    title: string
    key: string
    isSortable: boolean
}