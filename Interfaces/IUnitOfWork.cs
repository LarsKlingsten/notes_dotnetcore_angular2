using System.Threading.Tasks;
using notes.Persistence;

namespace notes.Interfaces {

    public interface IUnitOfWork {
        Task SaveChangesAsync();
    }

}