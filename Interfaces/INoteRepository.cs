using System.Collections.Generic;
using System.Threading.Tasks;
using notes.Models;
using notes.Persistence;

namespace notes.Interfaces {

    public interface INoteRepository {

        Task<Note> Note(int Id, bool InclRelated = true);
        Task<List<Note>> Notes();
        List<Note> NotesByKeywordId(int keywordId);
        List<Note> NotesByKeyword(string keywordName);

        void Add(Note note);
        void Remove(Note note);
    }
}