using System.Collections.Generic;
using System.Threading.Tasks;
using notes.Models;
using notes.Persistence;

namespace notes.Interfaces {

    public interface IKeywordRepository {
        Task<Keyword> Keyword(int Id);
        Task<List<Keyword>> Keywords();

        void Add(Keyword k);
        void Remove(Keyword k);
    }
}