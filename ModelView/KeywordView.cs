using System;
using System.ComponentModel.DataAnnotations;

namespace notes.ModelView {

    public class KeywordView {
        [Range(0, Int32.MaxValue)]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }

}