namespace notes.ModelView {

    public class NoteKeywordView {
        public int NoteId { get; set; }  
        public int KeywordId { get; set; }    
    }
}