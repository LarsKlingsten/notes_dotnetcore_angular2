using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace notes.ModelView {

    //used for View
    public class NoteView {
        [Range(-1, Int32.MaxValue)]
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public ICollection<KeywordView> Keywords { get; set; } // Icollection is must for entity framework

        NoteView() {
            this.Keywords = new Collection<KeywordView>();
        }
    }
}
