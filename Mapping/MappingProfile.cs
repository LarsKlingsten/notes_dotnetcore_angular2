using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using notes.Models;
using notes.ModelView;

namespace notes.Mapping {
    public class MappingProfile : Profile {

        // automap by matching fields in the 2 classes
        // maps are unidirectional -> note -> NoteView (but not the reverse direction)
        public MappingProfile() {

            //domain (database) to API (viewModel) 
            // CreateMap<Note, NoteView >()
            // .ForMember(nv => nv.Keywords, opt => opt.MapFrom(n => n.Keywords.Select(nk => nk.KeywordId)));

            CreateMap<Note, NoteView>()
            .ForMember(nv => nv.Keywords, opt => opt.MapFrom(n => n.Keywords
            .Select(nk => new KeywordView { Id = nk.Keyword.Id, Name = nk.Keyword.Name })));


            CreateMap<Keyword, KeywordView>();
            CreateMap<NoteKeyword, NoteKeywordView>();

            //api (viewModel) to domain (database) resource
            CreateMap<KeywordView, Keyword>()
            .ForMember(k => k.Id, opt => opt.Ignore()); // ignore nv.Id for DB updates;

            CreateMap<NoteView, Note>()
            .ForMember(n => n.Id, opt => opt.Ignore()) // ignore nv.Id for DB updates
            .ForMember(n => n.Keywords, opt => opt.Ignore()) // instead use below AfterMap
            .AfterMap((nv, n) => {
                // if database DOES include keywords that are not included 
                // in the API request -> remove them
                var removedKeywords = new List<NoteKeyword>();
                foreach (var noteKeyword in n.Keywords) {
                    if (!nv.Keywords.Any(x => x.Id == noteKeyword.KeywordId)) {
                        removedKeywords.Add(noteKeyword);
                    }
                }
                foreach (var k in removedKeywords) { // now remove the keywords
                    n.Keywords.Remove(k);
                }

                // if database DOES NOT include API request keywords -> add them
                foreach (var keyboard in nv.Keywords) {
                    if (!n.Keywords.Any(k => k.KeywordId == keyboard.Id)) {
                        n.Keywords.Add(new NoteKeyword { KeywordId = keyboard.Id, NoteId = n.Id });
                    }
                }
            });
        }
    }

}
