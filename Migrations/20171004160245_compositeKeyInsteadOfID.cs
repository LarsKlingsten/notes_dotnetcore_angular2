﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace notes.Migrations
{
    public partial class compositeKeyInsteadOfID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NoteKeyword_Keywords_KeywordId",
                table: "NoteKeyword");

            migrationBuilder.DropForeignKey(
                name: "FK_NoteKeyword_Notes_NoteId",
                table: "NoteKeyword");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NoteKeyword",
                table: "NoteKeyword");

            migrationBuilder.DropIndex(
                name: "IX_NoteKeyword_NoteId",
                table: "NoteKeyword");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "NoteKeyword");

            migrationBuilder.RenameTable(
                name: "NoteKeyword",
                newName: "NoteKeywords");

            migrationBuilder.RenameIndex(
                name: "IX_NoteKeyword_KeywordId",
                table: "NoteKeywords",
                newName: "IX_NoteKeywords_KeywordId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NoteKeywords",
                table: "NoteKeywords",
                columns: new[] { "NoteId", "KeywordId" });

            migrationBuilder.AddForeignKey(
                name: "FK_NoteKeywords_Keywords_KeywordId",
                table: "NoteKeywords",
                column: "KeywordId",
                principalTable: "Keywords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NoteKeywords_Notes_NoteId",
                table: "NoteKeywords",
                column: "NoteId",
                principalTable: "Notes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NoteKeywords_Keywords_KeywordId",
                table: "NoteKeywords");

            migrationBuilder.DropForeignKey(
                name: "FK_NoteKeywords_Notes_NoteId",
                table: "NoteKeywords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NoteKeywords",
                table: "NoteKeywords");

            migrationBuilder.RenameTable(
                name: "NoteKeywords",
                newName: "NoteKeyword");

            migrationBuilder.RenameIndex(
                name: "IX_NoteKeywords_KeywordId",
                table: "NoteKeyword",
                newName: "IX_NoteKeyword_KeywordId");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "NoteKeyword",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NoteKeyword",
                table: "NoteKeyword",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_NoteKeyword_NoteId",
                table: "NoteKeyword",
                column: "NoteId");

            migrationBuilder.AddForeignKey(
                name: "FK_NoteKeyword_Keywords_KeywordId",
                table: "NoteKeyword",
                column: "KeywordId",
                principalTable: "Keywords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NoteKeyword_Notes_NoteId",
                table: "NoteKeyword",
                column: "NoteId",
                principalTable: "Notes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
