﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace notes.Migrations
{
    public partial class nothing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "KeywordId",
                table: "Notes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Notes_KeywordId",
                table: "Notes",
                column: "KeywordId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_Keywords_KeywordId",
                table: "Notes",
                column: "KeywordId",
                principalTable: "Keywords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notes_Keywords_KeywordId",
                table: "Notes");

            migrationBuilder.DropIndex(
                name: "IX_Notes_KeywordId",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "KeywordId",
                table: "Notes");
        }
    }
}
