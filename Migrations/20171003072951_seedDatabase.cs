﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace notes.Migrations {
    public partial class seedDatabase : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {

            var updated = "'" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";

            migrationBuilder.Sql($"insert into notes (body, subject, created, updated) values ('about c#', 'note c#',{updated},{updated})");
            migrationBuilder.Sql($"insert into notes (body, subject, created, updated) values ('about SQL', 'note SQL',{updated},{updated})");
            migrationBuilder.Sql($"insert into notes (body, subject, created, updated) values ('about SQL and C#', 'note SQL and C#',{updated},{updated})");

            migrationBuilder.Sql($"insert into keywords (Name, Created, updated) values ('programming', {updated},{updated})");
            migrationBuilder.Sql($"insert into keywords (Name, Created, updated) values ('CSharp', {updated},{updated})");
            migrationBuilder.Sql($"insert into keywords (Name, Created, updated) values ('Golang', {updated},{updated})");
            migrationBuilder.Sql($"insert into keywords (Name, Created, updated) values ('SQL', {updated},{updated})");
            migrationBuilder.Sql($"insert into keywords (Name, Created, updated) values ('Python', {updated},{updated})");

            migrationBuilder.Sql("insert into noteKeyword (noteId, keywordId) values ((select id from notes where subject = 'note c#'), (select id from keywords where name = 'C#')) ");
            migrationBuilder.Sql("insert into noteKeyword (noteId, keywordId) values ((select id from notes where subject = 'note SQL'), (select id from keywords where name = 'SQL')) ");
            migrationBuilder.Sql("insert into noteKeyword (noteId, keywordId) values ((select id from notes where subject = 'note SQL and C#'), (select id from keywords where name = 'C#')) ");
            migrationBuilder.Sql("insert into noteKeyword (noteId, keywordId) values ((select id from notes where subject = 'note SQL and C#'), (select id from keywords where name = 'SQL')) ");
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.Sql("delete from note where subject in ('about c#' 'about SQL', 'about SQL and C#')");
            migrationBuilder.Sql("delete from keywords where name in ('programming' 'c#', 'Golang', 'SQL', 'Python')");
        }
    }
}
