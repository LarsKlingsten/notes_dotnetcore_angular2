using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace notes.Models {

    [Table("Keywords")]
    public class Keyword {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}