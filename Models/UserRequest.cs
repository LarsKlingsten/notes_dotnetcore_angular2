using Newtonsoft.Json;

namespace notes.Models {
    public class UserRequest {
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Query { get; set; }


        public const string url = "http://localhost:3344/sql/v1/query";
        public string ToJSON() {
            return JsonConvert.SerializeObject(this);
        }
    }
}