using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace notes.Models {

    public class Note {

        public int Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public ICollection<NoteKeyword> Keywords { get; set; } // Icollection is must for entity framework

        public Note() {
            this.Keywords = new Collection<NoteKeyword>();
        }
    }
}