using System.ComponentModel.DataAnnotations.Schema;

namespace notes.Models {
  
  
  [Table("NoteKeywords")]
    public class NoteKeyword {

        public int NoteId { get; set; } // many to many relationship
        public Note Note { get; set; }    // requires both ID and the Class (for entity framework)

        // must use this naming convention 'Keyword' + 'KeywordId' 
        // so EF knows it refers to the same obejct 
        // (otherwise EF makes it own)
        public int KeywordId { get; set; }     // foreign key 
        public Keyword Keyword { get; set; }

    }
} 