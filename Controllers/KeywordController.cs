using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;

using notes.Models;
using notes.Persistence;
using notes.ModelView;
using notes.Interfaces;
using Klingsten.Snippet;
using Klingsten.Snippet.Service;
using Klingsten.Snippet.Models;

namespace notes.Controllers {

    [Route("api/keywords")]
    public class KeywordController : Controller {

        private readonly IMapper mapper;
        private readonly IKeywordRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public KeywordController(IMapper mapper, IKeywordRepository repository, IUnitOfWork unitOfWork) {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet()]
        public async Task<IActionResult> GetKeywords() {
            var keywords = await repository.Keywords();
            var KeywordViews = mapper.Map<List<Keyword>, List<KeywordView>>(keywords);
            var resp = ServerResponse.OkResponse("Success. Retrieved Keywords");
            resp.data = KeywordViews;
            return Ok(resp);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetKeyword(int id) {
            var keyword = await repository.Keyword(id);
            if (keyword == null) {
                return NotFound(ServerResponse.RecordNotFound());
            }
            var resp = ServerResponse.OkResponse("Success. Retrieved Keyword");
            resp.data = mapper.Map<Keyword, KeywordView>(keyword);
            return Ok(resp);
        }

        [HttpPost] // post -> api/keywords
        public async Task<IActionResult> Create([FromBody] KeywordView keywordView) {
            if (!ModelState.IsValid) {
                return BadRequest(ServerResponse.BadModelState());
            }

            var keyword = mapper.Map<KeywordView, Keyword>(keywordView);
            repository.Add(keyword);
            try {
                await unitOfWork.SaveChangesAsync();
            } catch (Exception e) {
                return BadRequest(ServerResponse.DataBaseError(e.InnerException.Message));
            }

            keyword = await repository.Keyword(keyword.Id);
            var keywordViewResult = mapper.Map<Keyword, KeywordView>(keyword);

            var resp = ServerResponse.OkResponse($"Success. Created new Record with id={keywordViewResult.Id}");
            resp.data = keywordViewResult;
            resp.id = keywordViewResult.Id;
            return Ok(resp);
        }

        [HttpPut("{id}")] // PUT -> api/keywords/1
        public async Task<IActionResult> Update(int id, [FromBody] KeywordView KeywordView) { // id come from route, noteview from the request body
            if (!ModelState.IsValid) {
                return BadRequest(ServerResponse.BadModelState());
            }

            var keyword = await repository.Keyword(id);
            if (keyword == null) {
                return NotFound(ServerResponse.RecordNotFound());
            }
            mapper.Map<KeywordView, Keyword>(KeywordView, keyword);
            try {
                await unitOfWork.SaveChangesAsync();
            } catch (Exception e) {
                return BadRequest(ServerResponse.DataBaseError(e.InnerException.Message));
            }
            keyword = await repository.Keyword(keyword.Id);
            var keywordViewResult = mapper.Map<Keyword, KeywordView>(keyword);

            var resp = ServerResponse.OkResponse($"Server:Success. Updated Keyword. id={id}");
            resp.data = keywordViewResult;
            resp.id = id;
            return Ok(resp);
        }



        [HttpDelete("{id}")] // ->Delete: api/keyword/1
        public async Task<IActionResult> Delete(int id) { // id come from route, noteview from the request body
            var keyword = await repository.Keyword(id);
            if (keyword == null) {
                return NotFound($"Could not delete keyword with id={id}. Record was not found");
            }
            repository.Remove(keyword);
            try {
                await unitOfWork.SaveChangesAsync();
            } catch (Exception e) {
                var r = ServerResponse.DataBaseError(e.InnerException.Message);
                r.msg = $"Could not delete keyword id={id}";
                return BadRequest(r);
            }
            var resp = ServerResponse.OkResponse($"Success. Deleted Keyword. id={id}");
            resp.id = id;
            return Ok(resp);
        }
    }
}