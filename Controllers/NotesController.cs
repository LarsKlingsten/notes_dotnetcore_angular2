using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;

using notes.Models;
using notes.Persistence;
using notes.ModelView;
using notes.Interfaces;
using Klingsten.Snippet;
using Klingsten.Snippet.Service;
using Klingsten.Snippet.Models;

namespace notes.Controllers {

    [Route("/api/Notes")]
    public class NotesController : Controller {
        private readonly IMapper mapper;
        private readonly INoteRepository repository;
        private readonly IMyHttpService httpService;
        private readonly IUnitOfWork unitOfWork;

        public NotesController(IMapper mapper, INoteRepository repository, IMyHttpService http, IUnitOfWork unitOfWork) {
            this.unitOfWork = unitOfWork;
            this.httpService = http;
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateNote([FromBody] NoteView noteViewSave) {
            if (!ModelState.IsValid) {
                return BadRequest(ServerResponse.BadModelState());
            }

            var note = mapper.Map<NoteView, Note>(noteViewSave);
            note.Updated = DateTime.UtcNow;
            note.Created = DateTime.UtcNow;
            repository.Add(note);
            try {
                await unitOfWork.SaveChangesAsync();
            } catch (Exception e) {
                return BadRequest(ServerResponse.DataBaseError(e.InnerException.Message));
            }

            note = await repository.Note(note.Id);
            var noteViewResult = mapper.Map<Note, NoteView>(note);

            var resp = ServerResponse.OkResponse($"Success. Created new Record with id={noteViewResult.Id}");
            resp.data = noteViewResult;
            resp.id = noteViewResult.Id;
            return Ok(resp);
        }

        [HttpPut("{id}")] // PUT -> api/notes/1
        public async Task<IActionResult> UpdateNote(int id, [FromBody] NoteView noteView) { // id come from route, noteview from the request body
            if (!ModelState.IsValid) {
                return BadRequest(ServerResponse.BadModelState());
            }

            var note = await repository.Note(id);
            if (note == null) {
                return NotFound(ServerResponse.RecordNotFound());
            }
            mapper.Map<NoteView, Note>(noteView, note); // source->dest (using noteView to populate "note" fm database)
            note.Updated = DateTime.UtcNow;
            try {
                await unitOfWork.SaveChangesAsync(); // @MappingProfile.cs remember to add  .ForMember(k => k.Id, opt => opt.Ignore());  // we cant change Id and we dont want to
            } catch (Exception e) {
                return BadRequest(ServerResponse.DataBaseError(e.InnerException.Message));
            }
            note = await repository.Note(note.Id);
            var noteViewResult = mapper.Map<Note, NoteView>(note);

            var resp = ServerResponse.OkResponse($"Saved note '{note.Subject}'");
            resp.data = noteViewResult;
            resp.id = id;
            return Ok(resp);
        }

        [HttpDelete("{id}")] // ->Delete: api/notes/1
        public async Task<IActionResult> DeleteNote(int id) { // id come from route, noteview from the request body
            var note = await repository.Note(id, InclRelated: false);
            if (note == null) {
                return NotFound($"Could not delete record with id={id}. Record was not found");
            }

            repository.Remove(note);
            try {
                await unitOfWork.SaveChangesAsync();
            } catch (Exception e) {
                var r = ServerResponse.DataBaseError(e.InnerException.Message);
                r.msg = $"Could not delete record id={id}";
                return BadRequest(r);
            }
            var resp = ServerResponse.OkResponse($"Success. Deleted Note. id={id}");
            resp.id = id;
            return Ok(resp);
        }

        [HttpGet()]
        public async Task<IActionResult> GetNotes() {
            var notes = await repository.Notes();
            var noteViews = mapper.Map<List<Note>, List<NoteView>>(notes);
            var resp = ServerResponse.OkResponse($"Success retrieving data");
            resp.data = noteViews;
            return Ok(resp);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetNote(int id) {
            var note = await repository.Note(id);
            if (note == null) {
                return NotFound(ServerResponse.RecordNotFound());
            }
            var noteView = mapper.Map<Note, NoteView>(note);

            var resp = ServerResponse.OkResponse($"Success retrieving data");
            resp.data = noteView;
            return Ok(resp);
        }

        [HttpGet("keyword/{id}")]
        public IActionResult GetNotesByKeyword(int id) {
            var notes = repository.NotesByKeywordId(id);
            if (notes.Count == 0) {
                return NotFound(ServerResponse.RecordNotFound($"Could find the any notes with KeywordID={id}."));
            }
            var noteViews = mapper.Map<List<Note>, List<NoteView>>(notes);

            var resp = ServerResponse.OkResponse($"Success retrieving data");
            resp.data = noteViews;
            return Ok(resp);
        }

        [HttpGet("keywordname/{name}")]
        public IActionResult GetNoteByKeywordName(string name) {

            var notes = repository.NotesByKeyword(name);
            if (notes.Count == 0) {
                return NotFound(ServerResponse.RecordNotFound($"Could find the any notes with the key '{name}'."));
            }
            var noteViews = mapper.Map<List<Note>, List<NoteView>>(notes);

            var resp = ServerResponse.OkResponse($"Success retrieving data");
            resp.data = noteViews;
            return Ok(resp);
        }

        /// <Summary>
        /// uses HttpService (via seperate JSON-TO-SQL_BRIDGE service) to test difference in performance
        /// (see . ~/source/golang/src/larsklingsten/jsonToSqlBrigde -> go run main.go) 
        /// DO NOT USE THIS FUNCTION - AS IT DOES NOT HANDLE SQL INJECTION
        /// </Summary>
        [HttpGet("keywordname-go/{name}")]
        public async Task<IActionResult> GetNoteByKeywordNameGo(string name) {
            const string url = UserRequest.url; //"http://localhost:3344/sql/v1/query";
            var userRequest = new UserRequest
            {
                User = "read",
                Password = "secret",
                Database = "note",
                Query = $"select n.*, k.name from notes n  inner join NoteKeywords nk on nk.NoteId = n.Id  inner join keywords k on nk.KeywordId = k.id where k.name = '{name}'"
            };

            try {
                var httpRes = await httpService.PutWithBody(url, userRequest.ToJSON());
                var resp = ServerResponse.OkResponse($"Success retrieving data");
                resp.data = httpRes;
                return Ok(resp);
            } catch (Exception e) {
                return BadRequest(ServerResponse.BadResponse(e.Message));
            }
        }

    }
}