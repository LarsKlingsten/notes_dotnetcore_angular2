using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using notes.Interfaces;
using notes.Models;
using notes.Persistence;
using System.Linq;
using System;

namespace notes.Persistence {

    /// <Summary>
    /// The class should be seen a being similar to List as using objects in memory
    /// It does therefore not have an Update or SaveChanges methods
    /// -> this is instead handled by the Database single method "SaveChangesAsync"  
    /// </Summary>
    public class KeywordRepository : IKeywordRepository {

        private readonly NoteDbContext context;

        public KeywordRepository(NoteDbContext context) {
            this.context = context;
        }

        public async Task<Keyword> Keyword(int id) {
            return await context.Keywords.FindAsync(id);
        }

        public async Task<List<Keyword>> Keywords() {
            return await context.Keywords.OrderBy(t => t.Name).ToListAsync();
        }

         public void Add(Keyword k) {
            context.Keywords.Add(k);
        }

        public void Remove(Keyword k) {
            context.Keywords.Remove(k);
        }
    }
}