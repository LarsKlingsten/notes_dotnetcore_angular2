using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using notes.Interfaces;
using notes.Models;
using notes.Persistence;
using System.Linq;
using System;

namespace notes.Persistence {

    /// <Summary>
    /// The class should be seen a being similar to List as using objects in memory
    /// It does therefore not have an Update or SaveChanges methods
    /// -> this is instead handled by the Database single method "SaveChangesAsync"  
    /// </Summary>
    public class NoteRepository : INoteRepository {

        private readonly NoteDbContext context;

        public NoteRepository(NoteDbContext context) {
            this.context = context;
        }

        public async Task<Note> Note(int id, bool InclRelated = false) {

            if (!InclRelated) {
                return await context.Notes.FindAsync(id);
            }
            return await context.Notes.Include(n => n.Keywords).ThenInclude(nk => nk.Keyword).SingleOrDefaultAsync(v => v.Id == id);
        }

        public async Task<List<Note>> Notes() {
            return await context.Notes.Include(n => n.Keywords).ThenInclude(nk => nk.Keyword).ToListAsync();
        }

        public List<Note> NotesByKeywordId(int keywordId) {
            var notes = from n in context.Notes.Include(n => n.Keywords).ThenInclude(nk => nk.Keyword)
                        join nk in context.NoteKeywords on n.Id equals nk.NoteId
                        where nk.KeywordId == keywordId
                        select n;

            return notes.ToList();
        }

        public List<Note> NotesByKeyword(string keywordName) {
            var notes = from n in context.Notes.Include(n => n.Keywords).ThenInclude(nk => nk.Keyword)
                        join nk in context.NoteKeywords on n.Id equals nk.NoteId
                        join k in context.Keywords on nk.KeywordId equals k.Id
                        where k.Name == keywordName
                        select n;

            return notes.ToList();
        }

        public void Add(Note note) {
            context.Notes.Add(note);
        }

        public void Remove(Note note) {
            context.Notes.Remove(note);
        }
    }
}