using System.Threading.Tasks;
using notes.Interfaces;

namespace notes.Persistence {

    ///<Summary> 
    /// Unit of Work Pattern. The Unit of Work pattern is used to group one or more operations 
    /// (usually database operations) into a single transaction or “unit of work”, so that 
    /// all operations either pass or fail as one.
    /// http://jasonwatmore.com/post/2015/01/28/unit-of-work-repository-pattern-in-mvc5-and-web-api-2-with-fluent-nhibernate-and-ninject
    ///</Summary>
    public class UnitOfWork : IUnitOfWork {

        private readonly NoteDbContext context;

        public UnitOfWork(NoteDbContext context) {
            this.context = context;
        }

        public async Task SaveChangesAsync() {
            await context.SaveChangesAsync();
        }
    }
}