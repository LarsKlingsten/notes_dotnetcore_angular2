using Microsoft.EntityFrameworkCore;
using notes.Models;

namespace notes.Persistence {

    public class NoteDbContext : DbContext {

        public DbSet<Note> Notes { get; set; }
        public DbSet<Keyword> Keywords { get; set; }
        public DbSet<NoteKeyword> NoteKeywords { get; set; }

        public NoteDbContext(DbContextOptions<NoteDbContext> options) : base(options) {

        }

        // fluentApi (=> override EF conventions) (alternative to data annotations)
        // create composite key for NoteKeyword (using noteId and KeywordId instead of ID as key)
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<NoteKeyword>().HasKey(vf => new { vf.NoteId, vf.KeywordId });
        }


    }
}