using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Klingsten.Snippet.Service;
using notes.Persistence;
using notes.Interfaces;

namespace notes {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add 
        //services (dependency injection) to the container.
        public void ConfigureServices(IServiceCollection services) {
            var connStr = Configuration.GetConnectionString("Default"); // see apppsettings.Json (that is magically loaded by dotnet core)

            // dependency injection -> we use scoped as addDbContext (set by EF) is scoped.
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<IKeywordRepository, KeywordRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMyHttpService, MyHttpService>();
            services.AddAutoMapper();
            services.AddDbContext<NoteDbContext>(options => options.UseSqlServer(connStr));


            // handling self reference errors when using JSON
            services.AddMvc().AddJsonOptions(options => {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

            // export ASPNETCORE_ENVIRONMENT=Development 
            // dotnet run 
            if (env.IsDevelopment()) {
                Console.WriteLine("we are running in Development");
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });


            } else {
                Console.WriteLine("we are running in production");
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
